#pragma warning(disable:4996) 

#include "CountCommand.h"

#include "../../ci/Context.h"
#include "DatabaseCIObject.h"
#include "../../util/Util.h"
#include "../dbms/IntegerDatabaseObject.h"
#include "../dbms/DoubleDatabaseObject.h"
#include "../dbms/StringDatabaseObject.h"
#include "WellFormat.h"
#include "CIUtil.h"

void CountCommand::help(std::ostream& out)
{
	out << "count <table_name> <search column n>" << "\t\t" << " Finding number of rows in table which columns has <search value>" << endl
		<< "<search_value>" << endl;

}

void CountCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}
	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <table_name>
	char* targs = arrayArgs;
	targs = skipws(targs);
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}
	string tableName = targs;

	// #2 <search_column n>
	targs += p;
	targs = skipws(targs);
	p = parseArg(targs, out, Command::numberCharValidator);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	int columnNumber = atoi(targs);

	// #3 <search_value>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing value and table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	string value = targs;

	targs += p;
	targs = skipws(targs);
	if (*targs != 0) {
		out << "There are extra symbols" << endl;
		delete[] arrayArgs;
		return;
	}
	delete[] arrayArgs;

	if (tableName == "") {
		out << "Missing table name" << endl;
		return;
	}

	Table* t = db->getTable(tableName);
	if (t == nullptr) {
		out << "There is no table with this name:" << args << endl;
		return;
	}
	TableStructure* ts = t->getTableStructure();

	if (columnNumber > ts->getColumnSize()) {
		out << "Column number must be < than columns in table" << endl;
		return;
	}

	Column* column = ts->getColumn(columnNumber);

	ColumnType type = column->getType();
	DatabaseObject* columnValue = convertStrToDbmsObj(column, out, value);
	if (columnValue == nullptr) {
		return;
	}

	VIterator<Row*>* it = t->iterateRows();
	int count = 0;
	while (it->hasNext()) {
		Row* row = it->next();
		DatabaseObject* obj = row->getColumn(columnNumber);
		if (obj != nullptr && *obj == *columnValue) {
			count++;
		}
	}
	out << "Number of rows in table with <search_value> in <search_column n> is :" << count << endl;

	delete it;
	deleteDbmsObj(columnValue);
}
