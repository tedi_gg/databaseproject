#pragma once
#include "../../ci/Command.h"

class DeleteCommand : public Command {
public:
	DeleteCommand() : Command("delete") {}
	//	DeleteCommand(const DeleteCommand& cmd) : Command(cmd) {}
	//	~DeleteCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);

};