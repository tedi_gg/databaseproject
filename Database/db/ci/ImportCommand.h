#pragma once

#include "../../ci/Command.h"

class ImportCommand : public Command {
public:
	ImportCommand() : Command("import") {}
	//	ImportCommand(const SaveasCommand& cmd) : Command(cmd) {}
	//	~ImportCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};

