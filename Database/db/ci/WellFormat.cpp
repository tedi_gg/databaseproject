#include <windows.h>
#include <stdio.h>
#include <conio.h>


#include "WellFormat.h"
#include "../../util/Util.h"


int* WellFormat::computeMaxColumnsSize()
{
	TableStructure* ts = table->getTableStructure();
	int columnSize = ts->getColumnSize();
	int* maxColumnSize = new int[columnSize+1];

	for (int i = 0; i <= columnSize; i++) {
		maxColumnSize[i] = 0;
	}

	if (rowsCount == 0) {
		return maxColumnSize;
	}
	
	it->beforeFirst();
	while (it->hasNext()) {
		Row* row = it->next();
		for (int i = 0; i < columnSize; i++) {
			DatabaseObject* obj = row->getColumn(i);
			string sval;
			if (obj == nullptr) {
				sval = "NULL";
			}
			else {
				sval = obj->toString();
			}
			int slen = sval.size();
			if (maxColumnSize[i+1] < slen) {
				maxColumnSize[i+1] = slen;
			}
		}
	}

	int rval = columnSize - 1;
	for (int i = 0; i < columnSize; i++) {
		rval += maxColumnSize[i];
	}

	maxColumnSize[0] = rval;

	return maxColumnSize;
}

string WellFormat::rowToString(Row* row,const int columnSize,const  int* maxColumnsSize) 
{
	string rval = "";
	for (int i = 0; i < columnSize; i++) {
		DatabaseObject* dbObj = row->getColumn(i);
		string strCol = (dbObj == nullptr) ? "NULL" : dbObj->toString();
		int size = strCol.size();
		if (i > 0) {
			rval.append("|");
		}

		rval.append(strCol);
		if (i + 1 < columnSize) {
			int diff = maxColumnsSize[i + 1] - size;
			while (diff > 0) {
				rval.append(" ");
				diff--;
			}
		}
	}

	return rval;
}


void WellFormat::print(istream& in, ostream& out) 
{
	if (rowsCount == 0) {
		out << "Rows count " << rowsCount << endl;
		return;
	}

	ConsoleSupport* cs = new ConsoleSupport();

	int* maxColumnsSize = computeMaxColumnsSize();
	it->beforeFirst();

	TableStructure* ts = table->getTableStructure();
	int columnSize = ts->getColumnSize();

	int consoleRows = cs->getConsoleRows();
	int consoleColumns = cs->getConsoleColums();

	if (consoleRows < rowsCount || consoleColumns < maxColumnsSize[0]) {
		out << "Iteractive varian " << rowsCount << endl;
		// commands
		// next, previous, left, right, exit and help

		vector<string> lines;
		while (it->hasNext()) {
			Row* row = it->next();
			string line = rowToString(row, columnSize, maxColumnsSize);
			lines.push_back(line);
		}

		bool needPaging = (consoleRows < rowsCount);
		bool needShifting = (consoleColumns < maxColumnsSize[0]);

		int beginRow = 0;
		int beginColumn = 0;

		string cmds[][5] = {
			// only needPaging first
			{ "next", "exit", "", "", "" },
			// only needPaging middle
			{ "next", "previous", "exit", "", "" },
			// only needPaging last
			{ "previous", "exit", "", "", "" },

			// only needShifting first
			{ "right", "exit", "", "", "" },
			// only needShifting middle
			{ "left", "right", "exit", "", "" },
			// only needShifting last
			{ "left", "exit", "", "", "" },

			// first, first
			{ "next", "right", "exit", "", "" },
			// middle, first
			{ "next", "previous", "right", "exit", "" },
			// last, first
			{ "previous", "right", "exit", "", "" },

			// first, middle
			{ "next", "left", "right", "exit", "" },
			// middle, middle
			{ "next", "previous", "left", "right", "exit" },
			// last, middle
			{ "previous", "left", "right", "exit", "" },

			// first, last
			{ "next", "left", "exit", "", "" },
			// middle, last
			{ "next", "previous", "left", "exit", "" },
			// last, last
			{ "previous", "left", "exit", "", "" }
		};

		string promps[] = {
			// only needPaging first
			"(next, exit)>",
			// only needPaging middle
			"(next, previous, exit)>",
			// only needPaging last
			"(previous, exit)>",

			// only needShifting first
			"(right, exit)>",
			// only needShifting middle
			"(left, right, exit)>",
			// only needShifting last
			"(left, exit)>",

			// first, first
			"(next, right, exit)>",
			// middle, first
			"(next, previous, right, exit)>",
			// last, first
			"(previous, right, exit)>",

			// first, middle
			"(next, left, right, exit)>",
			// middle, middle
			"(next, previous, left, right, exit)>",
			// last, middle
			"(previous, left, right, exit)>",

			// first, last
			"(next, left, exit)>",
			// middle, last
			"(next, previous, left, exit)>",
			// last, last
			"(previous, left, exit)>"
		};

		char* bufLine = new char[4096];

		while (true) {
			int endRow = beginRow + consoleRows - 1;
			int maxRow = (endRow < rowsCount) ? endRow : rowsCount;

			int endColumn = beginColumn + consoleColumns;

			for (int i = beginRow; i < maxRow; i++) {
				string str = lines[i];
				if (beginColumn == 0 && str.size() < endColumn) {
					out << str << endl;
				}
				else if (beginColumn == 0) {
					string s = str.substr(0, consoleColumns - 3);
					out << s << ">>>" << endl;
				}
				else if (str.size() < beginColumn) {
					out << "<<<" << endl;
				}
				else if (str.size() < endColumn-3) {
					string s = str.substr(beginColumn, str.size() - beginColumn);
					out << "<<<" << s << endl;
				}
				else {
					string s = str.substr(beginColumn, str.size() - endColumn - 6);
					out << "<<<" << s << ">>>" << endl;
				}
			}

			int caseIndex;
			if (needPaging && !needShifting) {
				// 0 - 2
				caseIndex = (beginRow == 0) ? 0 : (maxRow < rowsCount) ? 1 : 2;
			}
			else if (needShifting && !needPaging) {
				// 3 - 5
				caseIndex = 3 + (beginColumn == 0) ? 0 : (endColumn < maxColumnsSize[0]) ? 1 : 2;
			}
			else {
				// 6 - 14
				caseIndex = (beginRow == 0) ? 0 : (maxRow < rowsCount) ? 1 : 2;
				caseIndex += (beginColumn == 0) ? 6 : (endColumn < maxColumnsSize[0]) ? 9 : 12;
			}

			out << promps[caseIndex];
			in.getline(bufLine, 4096);

			string cmdLine = bufLine;
			trim(&cmdLine);
			if (cmdLine.size() == 0) {
				continue;
			}

			bool found = false;
			for (int i = 0; i < 5; i++) {
				if (cmdLine == cmds[caseIndex][i]) {
					found = true;
					break;
				}
			}

			if (found) {
				if (cmdLine == "next") {
					beginRow += consoleRows - 1;
				}
				else if (cmdLine == "previous") {
					beginRow -= consoleRows - 1;
					if (beginRow < 0) {
						beginRow = 0;
					}
				}
				else if (cmdLine == "left") {
					if (endColumn >= maxColumnsSize[0]) {
						beginColumn -= consoleColumns - 3;
					}
					else {
						beginColumn -= consoleColumns - 6;
					}
					if (beginColumn <= 0) {
						beginColumn = 0;
					}
				}
				else if (cmdLine == "right") {
					if (beginColumn == 0) {
						beginColumn += consoleColumns - 3;
					}
					else {
						beginColumn += consoleColumns - 6;
					}beginColumn += consoleColumns - 3;
				}
				else if (cmdLine == "exit") {
					break;
				}
			}
		}

	}
	else {
		out << "Rows count " << rowsCount << endl;

		while (it->hasNext()) {
			Row* row = it->next();
			string line = rowToString(row, columnSize, maxColumnsSize);
			out << line << endl;
		}
	}

	delete maxColumnsSize;
}