#pragma once

#include "../../ci/Command.h"

class AggregateCommand : public Command {
	
	//int
	static long sum(long a, long b) { return a + b; }
	static long product(long a, long b) { return a * b; }
	static long max(long a, long b) { return a < b ? b : a; }
	static long min(long a, long b) { return a < b ? a : b; }
	
	//double
	static double sum(double a, double b) { return a + b; }
	static double product(double a, double b) { return a * b; }
	static double max(double a, double b) { return a < b ? b : a; }
	static double min(double a, double b) { return a < b ? a : b; }


public:
	AggregateCommand() : Command("aggregate") {}
	//	AggregateCommand(const AggregateCommand& cmd) : Command(cmd) {}
	//	~AggregateCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);

};