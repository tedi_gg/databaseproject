#pragma once

#include "../../ci/Command.h"

class PrintCommand : public Command {
public:
	PrintCommand() : Command("print") {}
	//	PrintCommand(const SaveasCommand& cmd) : Command(cmd) {}
	//	~PrintCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};