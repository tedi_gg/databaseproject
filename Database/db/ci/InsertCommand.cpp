#pragma warning(disable :4996)
#include "../../ci/Context.h"
#include "InsertCommand.h"
#include "DatabaseCIObject.h"
#include "../../util/Util.h"
#include "CIUtil.h"
#include "../dbms/NilDatabaseObject.h"

void InsertCommand::help(std::ostream& out)
{
	out << "insert <table_name" << "\t\t" << "insert new row in table with current values"<<endl
		<< "<col1> <col2>..<coln>" << endl;
}

void InsertCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		cout << "There is no <name> and <file_name>" << endl;
		return;
	}

	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());
	
	// #1 <table_name>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}
	string tableName = targs;

	targs += p;
	targs = skipws(targs);

	Table* t = db->getTable(tableName);
	if (t == nullptr) {
		out << "There is no table with this name:" << args << endl;
		return;
	}

	vector<string> columns;
	while (*targs) {
		p = parseValueArg(targs, out);
		if (p == -1) {
			delete[] arrayArgs;
			return;
		}

		string value = targs;
		columns.push_back(value);

		targs += p;
		targs = skipws(targs);
	}

	delete[] arrayArgs;

	Row* row = new Row();
	TableStructure* ts = t->getTableStructure();
	int columnCounts = ts->getColumnSize();
	int min = (columns.size() < columnCounts) ? columns.size() : columnCounts;
	int index = 0;
	for (; index < min; index++) {
		Column* column = ts->getColumn(index);
		DatabaseObject* value = convertStrToDbmsObj(column, out, columns[index]);
		if (value == nullptr) {
			delete row;
			return;
		}
		row->addColumn(value);
	}

	for (; index < columnCounts; index++) {
		row->addColumn(NilDatabaseObject::getInstance());
	}
	t->addRow(row);
	dbObj->setModified();


}
