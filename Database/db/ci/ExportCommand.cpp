#pragma warning(disable:4996)
#include "../../ci/Context.h"
#include "ExportCommand.h"
#include "DatabaseCIObject.h"
void ExportCommand::help(std::ostream& out)
{
	out << "export <name>" << "\t" << "saving table in file" << endl
		<< "<file_name>" << "\t" << endl;

}

void ExportCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		cout << "There is no <name> and <file_name>" << endl;
		return;
	}
	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());
	int len = args.size();
	int p = 0;
	for (; p <len; p++) {
		if (arrayArgs[p] == ' ')
			break;
	}
	if (p < len) {
		arrayArgs[p] = 0;
	}
	string tableName = arrayArgs;
	string fileName = (p < len) ? arrayArgs + (p + 1) : nullptr;
	Table* t = db->getTable(tableName);
	if (t == nullptr) {
		cout << "There is no table with this <" << tableName << "> name" << endl;
		return;
	}
	if (fileName == "") {
		cout << "Missing <file_name>" << endl;
		return;
	}

	ofstream* ofs = new ofstream(fileName);
	t->save(tableName, *ofs);
}
