#pragma warning(disable:4996) 

#include "InnerjoinCommand.h"

#include "../../ci/Context.h"
#include "SelectCommand.h"
#include "DatabaseCIObject.h"
#include "../../util/Util.h"
#include "WellFormat.h"
#include "CIUtil.h"
void InnerjoinCommand::help(std::ostream& out)
{
	out << "innerjoin <table 1> <column n1>" << "\t\t" << "InnerJoins table 1 column n1 with table 2 column n2 and creates new table with the columns" << endl
		<< "<table 2> <column n2>" << endl;
}

void InnerjoinCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}
	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <table 1>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string tableName1 = targs;


	if (tableName1 == "") {
		out << "Missing table name" << endl;
		return;
	}

	// #2 <column n1>
	targs += p;
	targs = skipws(targs);
	p = parseArg(targs, out, Command::numberCharValidator);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	int columnNumber1 = atoi(targs);


	// #3 <table 2>
	targs += p;
	targs = skipws(targs);
	if(*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string tableName2 = targs;

	// #4 <column n2>
	targs += p;
	targs = skipws(targs);
	p = parseArg(targs, out, Command::numberCharValidator);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	int columnNumber2 = atoi(targs);

	delete[] arrayArgs;

	Table* t1 = db->getTable(tableName1);
	if (t1 == nullptr) {
		out << "There is no table with this name:" << args << endl;
		return;
	}
	TableStructure* ts1 = t1->getTableStructure();

	if (columnNumber1 > ts1->getColumnSize()) {
		out << "Column number must be < than columns in table" << endl;
		return;
	}

	Column* column1 = ts1->getColumn(columnNumber1);

	Table* t2 = db->getTable(tableName2);
	if (t2 == nullptr) {
		out << "There is no table with this name:" << args << endl;
		return;
	}
	TableStructure* ts2 = t2->getTableStructure();

	if (columnNumber2 > ts2->getColumnSize()) {
		out << "Column number must be < than columns in table" << endl;
		return;
	}

	Column* column2 = ts2->getColumn(columnNumber2);

	string ijTableName = tableName1 + "_InnerJoin_"+tableName2;
	Table* t3 = db->getTable(ijTableName);
	if (t3 != nullptr) {
		int tmpId = 1;
		string prefix = ijTableName + "_";
		string tName = prefix + "1";
		for (int i = 2; db->getTable(tName) != nullptr; tName = prefix + to_string(i++));	// Empty Block
		ijTableName = tName;
	}

	File dbFile = ctx.getOpenFile();
	string fname = ijTableName + ".dbt";
	string fileName = filePaths(dbFile, fname);

	t3 = new Table(ijTableName, new File(fileName), fname);
	TableStructure* ts3 = t3->getTableStructure();
	int columnCount1 = ts1->getColumnSize();
	for (int i = 0; i < columnCount1; i++) {
		Column* column = ts1->getColumn(i);
		ts3->addColumn(new Column(column->getName(), column->getType()));
	}
	int columnCount2 = ts2->getColumnSize();
	for (int i = 0; i < columnCount2; i++) {
		if (i != columnNumber2) {
			Column* column = ts2->getColumn(i);
			ts3->addColumn(new Column(column->getName(), column->getType()));
		}
	}

	int rowsCount = 0;
	VIterator<Row*>* it1 = t1->iterateRows();
	while (it1->hasNext()) {
		Row* row1 = it1->next();
		DatabaseObject* obj1 = row1->getColumn(columnNumber1);
		if (obj1 != nullptr && obj1->getType() != ColumnType::NIL) {
			VIterator<Row*>* it2 = t2->iterateRows();
			while (it2->hasNext()) {
				Row* row2 = it2->next();
				DatabaseObject* obj2 = row2->getColumn(columnNumber2);
				if (obj2 != nullptr && obj2->getType() != ColumnType::NIL && *obj1 == *obj2) {
					Row* row3 = new Row();
					for (int i = 0; i < columnCount1; i++) {
						DatabaseObject* obj = row1->getColumn(i);
						row3->addColumn((obj != nullptr) ? obj->clone() : nullptr);
					}
					for (int i = 0; i < columnCount2; i++) {
						if (i != columnNumber2) {
							DatabaseObject* obj = row2->getColumn(i);
							row3->addColumn((obj != nullptr) ? obj->clone() : nullptr);
						}
					}

					t3->addRow(row3);
					rowsCount++;
				}
			}

			delete it2;
		}
	}

	delete it1;
	db->addTable(t3);
	
	out << "Create table " << ijTableName << " for this join. Insert rows: " << rowsCount << endl;
	dbObj->setModified();

	

}
