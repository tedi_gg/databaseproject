#pragma warning(disable :4996)
#include "../../ci/Context.h"
#include "RenameCommand.h"
#include "DatabaseCIObject.h"
#include "../../util/Util.h"

void RenameCommand::help(std::ostream& out)
{
	out << "rename <old_name>" << "\t\t" << "rename the table with name <old_name> to table with name <new_name>" << endl
		<< "<new_name>" << "\t\t\t" << endl;
}

void RenameCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		cout << "There is no <table name> and <new table name>" << endl;
		return;
	}

	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <table_name>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}
	string tableName = targs;

	targs += p;
	targs = skipws(targs);

	// #2 <new_table_name>
	if (*targs == 0) {
		out << "Missing new table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}
	string newTableName = targs;

	targs += p;
	targs = skipws(targs);
	if (*targs != 0) {
		out << "There are extra symbols" << endl;
		delete[] arrayArgs;
		return;
	}

	delete[] arrayArgs;


	if (db->getTable(newTableName) != nullptr) {
		out << "There is table with " << newTableName << ". Cannot be rename table. " << tableName << endl;
		return;
	}

	Table* t = db->rename(tableName, newTableName);
	if (t == nullptr) {
		out << "Missing table with " << tableName << endl;
		return;
	}

	out << "Success." << endl;
	dbObj->setModified();

}
