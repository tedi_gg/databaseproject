#pragma once

#include "../../ci/Command.h"

class ShowTablesCommand : public Command {
public:
	ShowTablesCommand() : Command("showtables") {}
	//	ShowTablesCommand(const SaveasCommand& cmd) : Command(cmd) {}
	//	~ShowTablesCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};