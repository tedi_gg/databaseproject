#pragma once

#include "../../ci/Command.h"

class ExportCommand : public Command {
	ExportCommand() : Command("export") {}
	//	ExportCommand(const SaveasCommand& cmd) : Command(cmd) {}
	//	~ExportCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);


};