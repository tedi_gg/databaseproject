#pragma warning(disable:4996) 

#include "DeleteCommand.h"

#include "../../ci/Context.h"
#include "SelectCommand.h"
#include "DatabaseCIObject.h"
#include "../../util/Util.h"
#include "WellFormat.h"
#include "CIUtil.h"

void DeleteCommand::help(std::ostream& out)
{
	out << "<table_name>" << "\t \t" << " Deletes alll lines in table with name:table_name which column is <search_column> with value <search_val>" << endl <<
		"<search_column n> <seach_value>" << endl;
}

void DeleteCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}
	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <table_name>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string tableName = targs;

	// #2 <search_column n>
	targs += p;
	targs = skipws(targs);
	p = parseArg(targs, out, Command::numberCharValidator);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	int searchColumnNumber = atoi(targs);

	// #3 <search_value>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing value and table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	string searchValue = targs;

	delete[] arrayArgs;

	Table* t = db->getTable(tableName);
	if (t == nullptr) {
		out << "There is no table with this name:" << args << endl;
		return;
	}
	TableStructure* ts = t->getTableStructure();

	if (searchColumnNumber > ts->getColumnSize()) {
		out << "Column number must be < than columns in table" << endl;
		return;
	}

	Column* column = ts->getColumn(searchColumnNumber);

	DatabaseObject* columnValue = convertStrToDbmsObj(column, out, searchValue);
	if (columnValue == nullptr) {
		return;
	}

	VIterator<Row*>* it = t->iterateRows();
	int deletedRows = 0;
	while (it->hasNext()) {
		Row* row = it->next();
		DatabaseObject* obj = row->getColumn(searchColumnNumber);
		if (obj != nullptr && *obj == *columnValue) {
			it->remove();
			delete row;
			deletedRows++;
		}
	}

	out << "Deleted rows " << deletedRows << endl;

	if (deletedRows) {
		dbObj->setModified();
	}

	deleteDbmsObj(columnValue);
	delete it;

}
