#pragma once

#include "../../ci/Command.h"

class CountCommand : public Command {
public:
	CountCommand() : Command("count") {}
	//	CountCommand(const CountCommand& cmd) : Command(cmd) {}
	//	~CountCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);

};