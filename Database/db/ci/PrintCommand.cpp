#pragma warning(disable:4996) 

#include "../../ci/Context.h"
#include "PrintCommand.h"
#include "DatabaseCIObject.h"
#include "ConsoleSupport.h"
#include "WellFormat.h"
#include "../../util/Util.h"

void PrintCommand::help(std::ostream& out)
{
	out << "print <name>" << "\t\t" << "shows all line of table <name>" << endl;
}

void PrintCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}
	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <table_name>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string tableName = targs;
	targs += p;
	targs = skipws(targs);
	if (*targs != 0) {
		out << "There are extra symbols" << endl;
		delete[] arrayArgs;
		return;
	}
	delete[] arrayArgs;

	Table* t = db->getTable(args);
	if (t == nullptr) {
		cout << "There is no table with this name:" << args << endl;
		return;
	}
	
	WellFormat wf(t, t->getRowsCount(), t->iterateRows());
	wf.print(in, out);

}
