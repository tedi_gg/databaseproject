#pragma once

#include "../../ci/Command.h"

class AddColumnCommand : public Command {
public:
	AddColumnCommand() : Command("addcolumn") {}
	//	AddColumnCommand(const SaveasCommand& cmd) : Command(cmd) {}
	//	~AddColumnCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);

};