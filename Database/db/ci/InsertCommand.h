#pragma once
#include "../../ci/Command.h"

class InsertCommand : public Command {
public:
	InsertCommand() : Command("insert") {}
	//	InsertCommand(const InsertCommand& cmd) : Command(cmd) {}
	//	~InsertCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);

};