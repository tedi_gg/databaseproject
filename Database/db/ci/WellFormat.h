#pragma once
#include "../dbms/Table.h"
#include "ConsoleSupport.h"

class WellFormat {
private:
	Table* table;
	const int rowsCount;
	
	VIterator<Row*>* it;

	// Zero position are maximum size for all column + separator
	int* computeMaxColumnsSize();
	static string rowToString(Row* row,const int columnSize, const int* maxColumnSize);

public:

	WellFormat(Table* table, int rows,VIterator<Row*>* iterate) : table(table), rowsCount(rows), it(iterate){
	}

	~WellFormat() {
		delete it;
	}

	void print(istream& in, ostream& out);

};