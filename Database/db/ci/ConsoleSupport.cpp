#include "ConsoleSupport.h"

int ConsoleSupport::getConsoleRows() const
{
	CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
	SMALL_RECT srctWindow;
	HANDLE hstdout;
	hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

	if (!GetConsoleScreenBufferInfo(hstdout, &csbiInfo)) {

		cout << "Error: " << GetLastError() << endl;

	}
	return csbiInfo.srWindow.Bottom - csbiInfo.srWindow.Top + 1;
}

int ConsoleSupport::getConsoleColums() const
{
	CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
	SMALL_RECT srctWindow;
	HANDLE hstdout;
	hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

	if (!GetConsoleScreenBufferInfo(hstdout, &csbiInfo)) {

		cout << "Error: " << GetLastError() << endl;

	}
	return csbiInfo.srWindow.Right - csbiInfo.srWindow.Left;
}
