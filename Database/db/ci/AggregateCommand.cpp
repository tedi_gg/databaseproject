#pragma warning(disable:4996) 

#include "../../ci/Context.h"

#include "AggregateCommand.h"

#include "DatabaseCIObject.h"
#include "../../util/Util.h"
#include "CIUtil.h"
#include "../dbms/IntegerDatabaseObject.h"
#include "../dbms/DoubleDatabaseObject.h"

void AggregateCommand::help(std::ostream& out)
{
	out << "aggregate <table_name> <search_column n>" << "\t\t" << "Performs <operation> of <target_column n> of all rows in table which <search_column n> contains <search_value>" << endl
		<< "<search_value> <target_column n>" << "\t\t" << "Possible operations are:sum,product,maximum,minimum." << endl
		<< "<operation>" << endl;
}

void AggregateCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}
	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <table name>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string tableName = targs;


	if (tableName == "") {
		out << "Missing table name" << endl;
		return;
	}

	// #2 <search_column_n>
	targs += p;
	targs = skipws(targs);
	p = parseArg(targs, out, Command::numberCharValidator);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	int columnNumber = atoi(targs);

	// #3 <search_value>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing value and table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	string value = targs;


	// #4 <target_column_n>
	targs += p;
	targs = skipws(targs);
	p = parseArg(targs, out, Command::numberCharValidator);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	int targetColumnNumber = atoi(targs);

	// #5 <operation>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing value and table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	string operation = targs;
	if (operation != "sum" && operation != "product" && operation != "maximum" && operation != "minimum") {
		out << "Wrong operation" << endl;
		delete[] arrayArgs;
		return;
	}

	targs += p;
	targs = skipws(targs);
	if (*targs != 0) {
		out << "There are extra symbols" << endl;
		delete[] arrayArgs;
		return;
	}

	delete[] arrayArgs;

	Table* t = db->getTable(tableName);
	if (t == nullptr) {
		out << "There is no table with this name:" << args << endl;
		return;
	}
	TableStructure* ts = t->getTableStructure();

	if (columnNumber > ts->getColumnSize()) {
		out << "Column number must be < than columns in table" << endl;
		return;
	}

	Column* column = ts->getColumn(columnNumber);

	DatabaseObject* columnValue = convertStrToDbmsObj(column, out, value);
	ColumnType type = column->getType();
	if (columnValue == nullptr) {
		out << "Wrong operation or search value is null" << endl;
		return;
	}

	Column* columnTarget = ts->getColumn(targetColumnNumber);
	ColumnType targetType = columnTarget->getType();
	if (targetType == ColumnType::STRING || targetType == ColumnType::NIL) {
		out << "Wrong operation or search target value is null" << endl;
		return;
	}

	VIterator<Row*>* it = t->iterateRows();
	if (targetType == ColumnType::INT) {
		long (*aggregate)(long, long) = 0;
		if (operation == "sum") {
			aggregate = AggregateCommand::sum;
		}
		else if (operation == "product") {
			aggregate = AggregateCommand::product;
		} 
		else if (operation == "max") {
			aggregate = AggregateCommand::max;
		}
		else if (operation == "min") {
			aggregate = AggregateCommand::min;
		}

		int count = 0;
		long collect = 0;
		while (it->hasNext()) {
			Row* row = it->next();
			DatabaseObject* obj = row->getColumn(columnNumber);
			if (obj != nullptr && *obj == *columnValue) {
				obj = row->getColumn(targetColumnNumber);
				if (obj != nullptr && obj->getType() == ColumnType::INT) {
					if (count == 0) {
						collect = ((IntegerDatabaseObject*)obj)->getData();
					}
					else {
						collect = aggregate(collect, ((IntegerDatabaseObject*)obj)->getData());
					}
					count++;
				}
			}
		}

		if (count == 0) {
			out << "There is no mattched elemnts" << endl;
		}
		else {
			out << operation << " result is " << collect << endl;
		}
	}
	else if (targetType == ColumnType::DOUBLE) {
		double (*aggregate)(double, double) = 0;
		if (operation == "sum") {
			aggregate = AggregateCommand::sum;
		}
		else if (operation == "product") {
			aggregate = AggregateCommand::product;
		}
		else if (operation == "max") {
			aggregate = AggregateCommand::max;
		}
		else if (operation == "min") {
			aggregate = AggregateCommand::min;
		}

		int count = 0;
		double collect = 0;
		while (it->hasNext()) {
			Row* row = it->next();
			DatabaseObject* obj = row->getColumn(columnNumber);
			if (obj != nullptr && *obj == *columnValue) {
				string tmp = columnValue->toString();
				if (count == 0) {
					collect = ((DoubleDatabaseObject*)obj)->getData();
				}
				else {
					collect = aggregate(collect, ((DoubleDatabaseObject*)obj)->getData());
				}
				count++;
			}
		}
		if (count == 0) {
			out << "There is no mattched elemnts" << endl;
		}
		else {
			out << operation << " result is " << collect << endl;
		}
	}
	


}
