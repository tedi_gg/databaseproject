#pragma once

#include "../../ci/Command.h"

class DescribeCommand : public Command {
public:
	DescribeCommand() : Command("describe") {}
	//	DescribeCommand(const SaveasCommand& cmd) : Command(cmd) {}
	//	~DescribeCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};