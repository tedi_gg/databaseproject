#pragma once

#include "../../ci/CIObject.h"
#include "../dbms/Database.h"
class DatabaseCIObject : public CIObject {
private:
	Database* database;
	bool modified = false;

	void copy(const DatabaseCIObject&);
	void remove();
public:
	DatabaseCIObject(Database* database) : database(database) {}
	DatabaseCIObject(const DatabaseCIObject&);
	DatabaseCIObject& operator=(const DatabaseCIObject&);
	virtual ~DatabaseCIObject();

	bool save(File&, std::ostream&);
	bool isModified() { return modified; }

	Database* getDatabase() const { return database; }

	void setModified() { modified = true; }
};

