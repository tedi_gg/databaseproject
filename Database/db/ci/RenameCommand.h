#pragma once
#include "../../ci/Command.h"

class RenameCommand : public Command {
public:
	RenameCommand() : Command("rename") {}
	//	RenameCommand(const RenameCommand& cmd) : Command(cmd) {}
	//	~RenameCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);

};