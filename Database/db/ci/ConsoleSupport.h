#pragma once
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <iostream>
using namespace std;

class ConsoleSupport {
private:
public:
	int getConsoleRows() const;
	int getConsoleColums() const;
};