#pragma once

#include "../../ci/OpenHandler.h"
#include "DatabaseCIObject.h"
class DatabaseOpenHandler : public OpenHandler{
public:
	CIObject* open(File&);
	CIObject* createEmptyObject(File&);
};