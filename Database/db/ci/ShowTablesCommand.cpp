
#include "ShowTablesCommand.h"
#include "../../ci/Context.h"
#include "DatabaseCIObject.h"
void ShowTablesCommand::help(std::ostream& out)
{
	out << "showtables" << "\t\t\t\t" << "shows list of names of all opened tables" << endl;
}

void ShowTablesCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	for (VIterator<Table*>* it = db->iterateTables(); it->hasNext();) {
		Table* t = it->next();
		cout << t->getName() << endl;
	}

}
