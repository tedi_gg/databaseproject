#pragma once
#include "../../ci/Command.h"

class SelectCommand : public Command {
public:
	SelectCommand() : Command("select") {}
	//	SelectCommand(const SelectCommand& cmd) : Command(cmd) {}
	//	~SelectCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);

};