#include "../../ci/Context.h"

#include "DescribeCommand.h"
#include "DatabaseCIObject.h"
void DescribeCommand::help(std::ostream& out)
{
	out << "describe <name>" << "\t\t" << "shows information about types of columns in table with name: <name> " << endl;
}

void DescribeCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		out << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		out << "There is no table <name>" << endl;
		return;
	}

	Table* t = db->getTable(args);
	if (t == nullptr) {
		out << "There is no table with this name:" << args << endl;
		return;
	}

	out << "Table Structure of " << t->getName() << endl;

	int cid = 0;
	TableStructure* ts = t->getTableStructure();
	for (VIterator<Column*>* it = ts->iterator(); it->hasNext();) {
		
		Column* column = it->next();
		out << "[" << (cid++) << "] " << column->getName() << " : ";
		ColumnType ct = column->getType();

		switch (ct)
		{
		case ColumnType::NIL:
			cout << "Error: NilColumn, " << endl;
			break;
		case ColumnType::INT:
			out << "int" << endl;
			break;
		case ColumnType::DOUBLE:
			out << "double" << endl;
			break;
		case ColumnType::STRING:
			out << "string" << endl;
			break;
		default:
			break;
		}
	}

}
