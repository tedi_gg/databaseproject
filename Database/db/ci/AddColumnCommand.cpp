#pragma warning(disable:4996)

#include "AddColumnCommand.h"

#include "../../ci/Context.h"
#include "DatabaseCIObject.h"
#include "../dbms/Column.h"
#include "../../util/Util.h"
#include "CIUtil.h"
void AddColumnCommand::help(std::ostream& out)
{
	out << "addcolumn <table_name>"<<"\t\t"<<"add new collumn in <table_name>"<<endl
		<<"<column_name> <column_type>" <<"\t"<<"with column name and column type"<<endl;
}

void AddColumnCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{

	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}
	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <table name>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string tableName = targs;


	if (tableName == "") {
		out << "Missing table name" << endl;
		return;
	}

	//#2 <column_name>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string columnName = targs;

	//#3 <column_type>
	targs += p;
	targs = skipws(targs);

	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	
	string columnType = targs;

	targs += p;
	targs = skipws(targs);
	if (*targs != 0) {
		out << "There are extra symbols" << endl;
		delete[] arrayArgs;
		return;
	}

	delete[] arrayArgs;



	Table* t = db->getTable(tableName);
	Column* c = nullptr;

	if (columnType == "") {
		 c = new Column(columnName, ColumnType::NIL);
		t->addColumn(c);
		dbObj->setModified();

	}
	else {
		ColumnType type = ColumnType::NIL;
		if (columnType == "string") {
			type = ColumnType::STRING;
		}
		else if (columnType == "int") {
			type = ColumnType::INT;
		}else if (columnType == "double") {
			type = ColumnType::DOUBLE;
		}
		if (type == ColumnType::NIL){
			c = new Column(columnName, ColumnType::NIL);
			t->addColumn(c);
			dbObj->setModified();
		}
		c = new Column(columnName, type);
		t->addColumn(c);
		dbObj->setModified();

	}
}
