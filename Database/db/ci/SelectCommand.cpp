#pragma warning(disable:4996) 
#include "../../ci/Context.h"
#include "SelectCommand.h"
#include "DatabaseCIObject.h"
#include "../../util/Util.h"
#include "../dbms/IntegerDatabaseObject.h"
#include "../dbms/DoubleDatabaseObject.h"
#include "../dbms/StringDatabaseObject.h"
#include "WellFormat.h"
#include "CIUtil.h"

void SelectCommand::help(std::ostream& out)
{
	out << "select <column_n>" << "\t\t" << "prints all rows from <table_name> with <value> in <column n> " << endl;
	out << "\t<value> <table_name>" << endl;
}

void SelectCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}
	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <column_n>
	char* targs = arrayArgs;
	targs = skipws(targs);
	int p = parseArg(targs, out, Command::numberCharValidator);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	int columnNumber = atoi(targs);

	// #2 <value>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing value and table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	string value = targs;

	// #3 <table name>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string tableName = targs;
	targs += p;
	targs = skipws(targs);
	if (*targs != 0) {
		out << "There are extra symbols" << endl;
		delete[] arrayArgs;
		return;
	}
	delete[] arrayArgs;

	if (tableName == "") {
		out << "Missing table name" << endl;
		return;
	}

	Table* t = db->getTable(tableName);
	if (t == nullptr) {
		out << "There is no table with this name:" << args << endl;
		return;
	}
	TableStructure* ts = t->getTableStructure();
	
	if (columnNumber > ts->getColumnSize()) {
		out << "Column number must be < than columns in table" << endl;
		return;
	}

	Column* column = ts->getColumn(columnNumber);


	DatabaseObject* columnValue = convertStrToDbmsObj(column, out, value);
	if (columnValue == nullptr) {
		return;
	}

	VIterator<Row*>* it = t->iterateRows();
	vector<Row*> v;
	while (it->hasNext()) {
		Row* row = it->next();
		DatabaseObject* obj = row->getColumn(columnNumber);
		if (obj != nullptr && *obj == *columnValue) {
			v.push_back(row);
		}
	}

	delete it;
	deleteDbmsObj(columnValue);
	it = new VIterator<Row*>(&v);

	WellFormat wf(t, v.size(), it);
	wf.print(in, out);

}
