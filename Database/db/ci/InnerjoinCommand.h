#pragma once
#include "../../ci/Command.h"

class InnerjoinCommand : public Command {
public:
	InnerjoinCommand() : Command("innerjoin") {}
	//	InnerjoinCommand(const InnerjoinCommand& cmd) : Command(cmd) {}
	//	~InnerjoinCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);

};