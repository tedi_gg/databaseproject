#pragma once
#include "../../ci/Command.h"

class UpdateCommand : public Command {
public:
	UpdateCommand() : Command("update") {}
	//	UpdateCommand(const UpdateCommand& cmd) : Command(cmd) {}
	//	~UpdateCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);

};