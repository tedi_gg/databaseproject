#include <iostream>
#include <fstream>

#include "ImportCommand.h"
#include "../../util/Util.h"
#include "../../ci/Context.h"
#include "DatabaseCIObject.h"

using namespace std;

void ImportCommand::help(std::ostream& out)
{
	out << "import <file_name>" << "\t\t" << "imports table in database from <file_name>" << endl;
}

void ImportCommand::execute(std::istream& in, std::ostream& out , std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();
		
	if (args == "") {
		cout << "There is no <file_name>" << endl;
		return;
	}

	string file_path = filePaths(ctx.getOpenFile(),args);

	char* buffer = new char[4096];
	
	ifstream *fis = new ifstream(file_path);
	fis->getline(buffer,4096);
	fis->close();
	delete fis;

	if (!(*buffer)) {
		cout << "There is no file name " << endl;
		return;
	}
	
	string name = buffer;
	if (db->getTable(name)) {
		cout << "There is table with this name:" << name << endl;
		return;
	}

	File* file = new File(file_path);

	Table* table = new Table(name,file,args);
	table->open();
	db->addTable(table);

	dbObj->setModified();

	delete[] buffer;
}

