#pragma warning(disable:4996) 

#include "../../ci/Context.h"

#include "UpdateCommand.h"
#include "DatabaseCIObject.h"
#include "../../util/Util.h"
#include "CIUtil.h"

void UpdateCommand::help(std::ostream& out)
{
    out << "update <table_name>" << "\t\t" << " Updates all lines in table which column number n" << endl
        << "        <search_value>" << "\t\t" << " has got <search_value> changing <target_column n> " << endl
        << "<target_column n> <target_value>" << " has got value: <target_value>" << endl;
}

void UpdateCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}

	CIObject* ciOjct = ctx.getObject();

	if (ciOjct == nullptr) {
		cout << "Database is not opened" << endl;
		return;
	}

	DatabaseCIObject* dbObj = (DatabaseCIObject*)ciOjct;
	Database* db = dbObj->getDatabase();

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}
	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <table name>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string tableName = targs;
	

	if (tableName == "") {
		out << "Missing table name" << endl;
		return;
	}

	// #2 <search_column_n>
	targs += p;
	targs = skipws(targs);
	p = parseArg(targs, out, Command::numberCharValidator);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	int columnNumber = atoi(targs);

	// #3 <search_value>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing value and table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	string value = targs;


	// #4 <target_column_n>
	targs += p;
	targs = skipws(targs);
	p = parseArg(targs, out, Command::numberCharValidator);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	int targetColumnNumber = atoi(targs);

	// #5 <target_value>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing value and table name" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	string targetValue = targs;

	targs += p;
	targs = skipws(targs);
	if (*targs != 0) {
		out << "There are extra symbols" << endl;
		delete[] arrayArgs;
		return;
	}

	delete[] arrayArgs;

	Table* t = db->getTable(tableName);
	if (t == nullptr) {
		out << "There is no table with this name:" << args << endl;
		return;
	}
	TableStructure* ts = t->getTableStructure();

	if (columnNumber > ts->getColumnSize()) {
		out << "Column number must be < than columns in table" << endl;
		return;
	}

	Column* column = ts->getColumn(columnNumber);

	DatabaseObject* columnValue = convertStrToDbmsObj(column, out, value);
	if (columnValue == nullptr) {
		return;
	}

	Column* columnTarget = ts->getColumn(targetColumnNumber);
	DatabaseObject* targetColumnValue = convertStrToDbmsObj(columnTarget, out, targetValue);
	if (targetColumnValue == nullptr) {
		deleteDbmsObj(columnValue);
		return;
	}

	VIterator<Row*>* it = t->iterateRows();
	int updatedColumns = 0;
	while (it->hasNext()) {
		Row* row = it->next();
		DatabaseObject* obj = row->getColumn(columnNumber);
		if (obj != nullptr && *obj == *columnValue) {
			row->setColumn(targetColumnNumber, targetColumnValue->clone());
			updatedColumns++;
		}
	}

	if (updatedColumns) {
		dbObj->setModified();
	}

	out << "Updated columns " << updatedColumns << endl;

	deleteDbmsObj(columnValue);
	deleteDbmsObj(targetColumnValue);
	delete it;




}
