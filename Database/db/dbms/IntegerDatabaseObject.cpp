#include "IntegerDatabaseObject.h"

using namespace std;

string IntegerDatabaseObject::encoding()
{
	if (data == 0) {
		return "0";
	}

	string s("");
	long val = data;
	int pos = 0;
	if (val < 0) {
		s += "-";
		val = -val;
		pos = 1;
	}

	char* tmp = new char[2];
	tmp[1] = 0;
	
	while (val != 0) {
		int r = val % 10;
		tmp[0] = '0' + r;
		val /= 10;
		s.insert(pos, tmp);
	}

	return s;
}

void IntegerDatabaseObject::save(ostream& out)
{
	out << encoding();
}

string IntegerDatabaseObject::toString()
{
	return encoding();
}

IntegerDatabaseObject* IntegerDatabaseObject::load(string& str)
{
	const char* data = str.data();
	if (!(*data)) {
		return nullptr;
	}

	bool neg = *data == '-';
	if (neg || *data == '+') {
		data++;
	}

	long val = 0;
	while (*data) {
		if (*data >= '0' && *data <= '9') {
			long tval = val * 10 + (*data - '0');
			if (tval < val) { // More than max long value
				// We will return null.
				return nullptr;
			}
			val = tval;
		}
		else {
			// Incorect contet
			return nullptr;
		}

		data++;
	}

	if (neg) val *= -1;

	return new IntegerDatabaseObject(val);
}

int IntegerDatabaseObject::cmp(const DatabaseObject& dbObj2) const
{
	ColumnType type = dbObj2.getType();
	if (type == ColumnType::INT) {
		IntegerDatabaseObject* p = (IntegerDatabaseObject * ) &dbObj2;
		int otherData = p->getData();
		return (this->data < otherData) ? -1 : (this->data == otherData) ? 0 : 1;
	}

	if (type == ColumnType::NIL) {
		return 1;
	}
	return -1;
}
