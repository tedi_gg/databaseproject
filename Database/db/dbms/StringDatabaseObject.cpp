#include "StringDatabaseObject.h"

string StringDatabaseObject::encoding()
{
	string s("");
	char* tmpstr = new char[3];
	tmpstr[0] = '\\';
	tmpstr[2] = 0;
	const char* str = data.data();
	if (str != nullptr) {
		while ((tmpstr[1] = *str) != 0) {
			if (tmpstr[1] == '\\' || tmpstr[1] == '"') {
				s.append(tmpstr); // \"0
			}
			else { // c0
				s.append(tmpstr + 1);
			}

			str++;
		}
	}
	delete[] tmpstr;

	return s;
}


void StringDatabaseObject::save(ostream& out)
{
	out << "\"" << encoding() << "\"";

}

string StringDatabaseObject::toString()
{
	return string("\"") + encoding() + "\"";
}

StringDatabaseObject* StringDatabaseObject::load(string& str)
{
	const char* data = str.data();
	if (!(*data)) {
		return nullptr;
	}

	if (*data != '"') {
		return nullptr;
	}

	size_t pos = str.find_first_of('\\');
	if (pos == string::npos) {
		// missing
		// check for last one and get sub string
		if (data[str.size() - 1] == '"') {
			string sval = str.substr(1, str.size() - 2); // Remove first and last char - the are quote
			return new StringDatabaseObject(sval);
		}
		else {
			return nullptr;
		}
	}

	

	string sval(str.substr(1, pos - 1));
	data += pos + 1;
	sval.append(data, 1);
	data++;

	while (*data) {
		if (*data == '\\') {
			data++;
			if (*data) {
				sval.append(data, 1);
			}
			else {
				return nullptr; // wrong format after \ missing char.
			}
		}
		else if (*data == '"') {
			if (*(data + 1) != 0) {
				// Wrong format - there is extra chars after "
				return nullptr;
			}
		}
		else {
			sval.append(data, 1);
		}

		data++;
	}



	return new StringDatabaseObject(sval);
}


int StringDatabaseObject::cmp(const DatabaseObject& dbObj2) const
{
	ColumnType type = dbObj2.getType();
	if (type == ColumnType::STRING) {
		StringDatabaseObject* p = (StringDatabaseObject*)&dbObj2;
		string otherData = p->data;
		return this->data.compare(otherData);
	}

	return 1;
}
