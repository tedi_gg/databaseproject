#pragma once

#include "DatabaseObject.h"
#include "ColumnType.h"

using namespace std;

class NilDatabaseObject : public DatabaseObject {
private:
	static NilDatabaseObject* instance;

protected:
	int cmp(const DatabaseObject& dbObj1) const;

public:
	ColumnType getType() const {
		return ColumnType::NIL;
	}

	void save(ostream& out) {}

	string toString() { return "NULL"; }

	static DatabaseObject* getInstance() {
		return instance;
	}

	DatabaseObject* clone() {
		return this;
	}
	
};



