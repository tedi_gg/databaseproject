#include "TableStructure.h"

void TableStructure::cpy(const TableStructure& other)
{
	columns = new vector<Column*>();
	for (vector<Column*>::iterator it = other.columns->begin(); it < other.columns->end(); it++) {
		Column* t = *it;
		addColumn(new Column(*t));
	}
}
void TableStructure::rmv()
{
	for (vector<Column*>::iterator it = columns->begin(); it < columns->end(); ) {
		Column* t = *it;
		it = columns->erase(it);
		delete t;
	}

	delete columns;

}

TableStructure::TableStructure(const TableStructure& other)
{
	cpy(other);
}

TableStructure& TableStructure::operator=(const TableStructure& other)
{
	if (this != &other) {
		rmv();
		cpy(other);
	}
	return *this;
}

TableStructure::~TableStructure()
{
	rmv();
}
