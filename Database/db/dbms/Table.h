#pragma once

#include <string>
#include <fstream>

#include "TableStructure.h"
#include "DatabaseObject.h"
#include "Row.h";
#include "../../util/VIterator.h"
#include "NilDatabaseObject.h"
#include "../../util/File.h"

using namespace std;

class Table {
private:
	string name;
	File* f;
	string fileName;
	TableStructure* ts;
	vector<Row*>* rows;

	void copy(const Table&);
	void remove();
public:
	Table(const string name, File* f, const string fileName);
	Table(const Table&);
	Table& operator=(const Table&);
	~Table();

	string getName() const  { return name; }
	string getFileName() const {return fileName;}
	File* getFile() const { return f; }
	TableStructure* getTableStructure() const  { return ts; }
	int getRowsCount() const { return rows->size(); }
	void addRow(Row* row) { rows->push_back(row); }
	void deleteRow(const int number) { rows->erase(rows->begin() + number - 1); }
	VIterator<Row*>* iterateRows();

	void addColumn(Column* column) {
		ts->addColumn(column);
		int csize = ts->getColumnSize();
		for (vector<Row*>::iterator it = rows->begin(); it < rows->end(); it++) {
			Row* row = *it;
			while (row->size() < csize) {
				row->addColumn(NilDatabaseObject::getInstance());
			}
		}
	}

	void save(ofstream&);
	void save(string , ofstream&);
	void open();

	void setName(const string name) { this->name = name; }

	
};

