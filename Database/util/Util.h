#pragma once

#include <string>
#include <iostream>

#include "File.h"

using namespace std;

inline void trim(std::string* str) {
	if (str != nullptr) {
		str->erase(0, str->find_first_not_of(" \n\r\t"));
		if (str->size() > 0) {
			str->erase(str->find_last_not_of(" \n\r\t") + 1);
		}
	}
}


inline string filePaths(File& file, const string& args) {

	string file_path("");
#ifdef _WIN32
	if (args[0] == '\\' || args.size() > 3 && args[0] >= 'A' && args[0] <= 'Z' && args[1] == ':' && args[2] == '\\') {
		file_path += args;
	}
	else {
		string baseFileName = file.getFileFullName();
		std::size_t found = baseFileName.find_last_of("\\");
		if (found == string::npos) {
			file_path += args;
		}
		else {
			file_path += baseFileName.substr(0, found) + args;
		}
	}
#else
	if (args[0] == '/') {
		file_path += args;
	}
	else {
		string baseFileName = file.getFileFullName();
		std::size_t found = baseFileName.find_last_of("/");
		if (found == string::npos) {
			file_path += args;
		}
		else {
			file_path += baseFileName.substr(0, found) + args;
		}
	}
#endif

	return file_path;
}

inline int skipws(char* str, int p, int len) {
	if (p < 0) {
		p = 0;
	}
	if (p > len) {
		return len;
	}

	for (; p < len; p++) {
		char ch = str[p];
		if (ch != ' ' && ch != '\t' && ch != '\n' && ch != '\r') {
			return p;
		}
	}

	return len;
}

inline char* skipws(char* str) {
	if (str == nullptr) {
		return str;
	}

	char* p = str;
	while (*p && (*p == ' ' || *p == '\t' || *p == '\n' || *p == '\r')) {
		p++;
	}

	return p;
}


int parseArg(char* args, ostream& out);
int parseArg(char* args, ostream& out, bool (charValidator)(char, ostream&));

int parseValueArg(char* args, ostream& out);

