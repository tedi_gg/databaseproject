#pragma once

#include "../db/dbms/DatabaseObject.h"
#include "../db/dbms/IntegerDatabaseObject.h"
#include "../db/dbms/DoubleDatabaseObject.h"
#include "../db/dbms/StringDatabaseObject.h"


inline DatabaseObject* StringToDataBaseType(ColumnType type, string& val) {


	DatabaseObject* columnValue;
	switch (type)
	{
	case ColumnType::NIL:
		break;
	case ColumnType::INT:
		columnValue = IntegerDatabaseObject::load(val);
		break;
	case ColumnType::DOUBLE:
		columnValue = DoubleDatabaseObject::load(val);
		break;
	case ColumnType::STRING:
		columnValue = StringDatabaseObject::load(val);
		break;
	default:
		break;
	}

}