// DBProject.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include "ci/CommandInterpreter.h"
#include "db/ci/DatabaseOpenHandler.h"
#include "db/ci/ImportCommand.h"
#include "db/ci/ShowTablesCommand.h"
#include "db/ci/DescribeCommand.h"
#include "db/ci/PrintCommand.h"
#include "db/ci/SelectCommand.h"
#include "db/ci/AddColumnCommand.h"
#include "db/ci/UpdateCommand.h"
#include "db/ci/DeleteCommand.h"
#include "db/ci/InsertCommand.h"
#include "db/ci/InnerjoinCommand.h"
#include "db/ci/RenameCommand.h"
#include "db/ci/CountCommand.h"
#include "db/ci/AggregateCommand.h"

using namespace std;
int main()
{
    CommandInterpreter commandInterpreter(new DatabaseOpenHandler());
    commandInterpreter.addCommand(new ImportCommand());
    commandInterpreter.addCommand(new ShowTablesCommand());
    commandInterpreter.addCommand(new DescribeCommand());
    commandInterpreter.addCommand(new PrintCommand());
    commandInterpreter.addCommand(new SelectCommand());
    commandInterpreter.addCommand(new AddColumnCommand());
    commandInterpreter.addCommand(new UpdateCommand());
    commandInterpreter.addCommand(new DeleteCommand());
    commandInterpreter.addCommand(new InsertCommand());
    commandInterpreter.addCommand(new InnerjoinCommand());
    commandInterpreter.addCommand(new RenameCommand());
    commandInterpreter.addCommand(new CountCommand());
    commandInterpreter.addCommand(new AggregateCommand());


    commandInterpreter.mainLoop();


  

    
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
