#pragma once

#include "Command.h"

using namespace std;

class HelpCommand :
	public Command
{

	static void print(Command*, void*);

public:
	HelpCommand() : Command("help") {}

	void help(ostream&);
	void execute(istream&, ostream&, string&, string&, Context&);
};

