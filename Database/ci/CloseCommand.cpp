#include <fstream>

#include "CloseCommand.h"
#include "Context.h"
#include "../util/Util.h"
using namespace std;

void CloseCommand::help(std::ostream& out)
{
	out << "close" << "\t\t\t\t" << "Closes opened file" << endl;
}
void CloseCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string&args, Context&ctx)
{

	trim(&args);
	if (args == "help") {
		help(out);
		return;
	}
	if (args.size() > 0) {
		out << "Incorrect arguments" << endl;
		help(out);
		return;
	}

	if (!ctx.canOpenFile()) {
		out << "The operation cannot be performed. Missing Open Handler." << endl;
		return;
	}


	if (ctx.isOpened()) {
		File openFile = ctx.getOpenFile();
		string fileName = openFile.getFileName();
		// std::size_t found = args.find_last_of("/\\");
		if (ctx.getObject()->isModified()) {
			//out << "There is opened file " << endl;
			// TODO invoke save or discard changes.
			out << "There is opened file with changes do you want to save them?(y/n)" << endl;
			char c[2];
			in.getline(c, 2);
			if (c[0] == 'y' || c[0] == 'Y') {
				ctx.save(out);
			}
		}

		ctx.doClose();

		out << "Sucessfully closed " << fileName << endl;
	}

}
