#include "HelpCommand.h"
#include "Context.h"

void HelpCommand::print(Command* cmd, void* arg)
{
	std::ostream* out = (std::ostream *) arg;
	cmd->help(*out);
}

void HelpCommand::help(std::ostream&out)
{
	out << "help" << "\t\t\t\t" << "print this information" << endl;
}

void HelpCommand::execute(std::istream&in, std::ostream&out, std::string&cmd, std::string&args, Context&ctx)
{
	if (args == "help") {
		help(out);
		return;
	}
	out << "The following commands are supported:" << endl;
	ctx.visitCommands(&HelpCommand::print, &out);
	
}
