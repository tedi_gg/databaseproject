#pragma once

#include <iostream>
#include <fstream>

#include "../util/File.h"

using namespace std;

class CIObject
{
public:
	virtual ~CIObject() {};

	virtual bool save(File& , std::ostream& ) = 0;
	virtual bool isModified() = 0;
};

