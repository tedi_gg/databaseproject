#pragma once

#include "CIObject.h"

class OpenHandler
{
public:
	virtual CIObject* open(File& ) = 0;
	virtual CIObject* createEmptyObject(File& ) = 0;
};

