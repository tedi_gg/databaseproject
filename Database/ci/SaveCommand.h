#pragma once

#include "Command.h"
class SaveCommand :
	public Command
{
public:
	SaveCommand() : Command("save") {}
//	SaveCommand(const SaveCommand& cmd) : Command(cmd) {}
//	~SaveCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};

