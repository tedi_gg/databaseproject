#pragma once

#include "CommandInterpreter.h"
#include "CIObject.h"
#include "../util/File.h"
//#include "OpenHandler.h"
//
//#include <vector>
//
//

using namespace std;

class Context
{
	CommandInterpreter *ci;
	File* openedFile;
	CIObject* object;
	bool fExit = false;

public:
	Context(CommandInterpreter*);

	bool isExit() { return fExit; }
	void exit() { fExit = true; }

	void doOpen(const File& file, CIObject* object) {
		openedFile = new File(file);
		this->object = object;
	}

	void doClose();

	bool save(ostream& out);

	bool canOpenFile() { return (&(ci->getOpenHandler()) != nullptr); }
	bool isOpened() { return openedFile != nullptr;  }

	CIObject* getObject() const { return object; }
	File& getOpenFile() const  { return *openedFile;  }
	OpenHandler* getOpenHandler() const { return & (ci->getOpenHandler()); }
	
	void visitCommands(void (*visitor)(Command*, void*), void* arg);


};

