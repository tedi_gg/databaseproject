#pragma once

#include "Command.h"

class SaveasCommand :
	public Command
{
public:
	SaveasCommand() : Command("saveas") {}
//	SaveasCommand(const SaveasCommand& cmd) : Command(cmd) {}
//	~SaveasCommand() {};

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};

